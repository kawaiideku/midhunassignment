# python function to implement bracket matching
# push down automata


def match(bracketStr):
    # remove all whitespace
    bracketStr = bracketStr.split()
    bracketStr = "".join(bracketStr)
    brackets = {
        '{': '}',
        '[': ']',
        '(': ')'
    }
    # B represents blank or empty stack
    stack = ['B']
    for i in bracketStr:
        if i in brackets.keys():
            stack.append(i)
        elif i in brackets.values():
            #exception handle 
            try:
                #check if end of stack is matching
                if brackets[stack[-1]] == i:
                    stack.pop()
            except:
                pass
    return stack == ['B']


print(match("( [ ] )"))
print(match("[ { } ( ) ]"))
print(match("( ( ] ]"))
print(match("} [ ] {"))
print(match("{ ( } )"))
