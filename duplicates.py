#python function to find common elements in 2 lists
def find_duplicates(x_list, y_list):
	dup_list = []
	for i in x_list:
		if i in y_list:
			dup_list.append(i)
	dup_list.sort()
	return dup_list

print(find_duplicates([1, 2, 3], [2, 4, 5]))
print(find_duplicates([7, 2, 5], [2, 4, 7, 8]))
print(find_duplicates([1, 5, 2, 6], [8, 9, 10]))